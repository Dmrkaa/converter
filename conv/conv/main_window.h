#pragma once
#include <Windows.h>
namespace conv {

	using namespace std;
	using namespace System;
	using namespace System::Web;
	using namespace System::Net;
	using namespace System::IO;
	using namespace System::Xml;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Collections::Generic;
	using namespace System::Net::NetworkInformation;


	/// <summary>
	/// ������ ��� main_window
	/// </summary>
	public ref class main_window : public System::Windows::Forms::Form
	{
	public:
		main_window(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~main_window()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  close_button;
	private: System::Windows::Forms::ComboBox^  transfer_from;
	private: System::Windows::Forms::ComboBox^  transfer_to;
	private: System::Windows::Forms::TextBox^  transfer_from_value;
	private: System::Windows::Forms::Label^  program_name;
	private: System::Windows::Forms::Label^  label_from;
	private: System::Windows::Forms::Label^  label_to;
	private: System::Windows::Forms::Button^  convert;
	private: System::Windows::Forms::Button^  hide_button;
	private: System::Windows::Forms::TextBox^  transfer_to_result;
	private: System::Windows::Forms::Label^  update_info;
	private: System::Windows::Forms::Label^  update_label;
	private: System::Windows::Forms::NotifyIcon^  tray_bar;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::ComponentModel::IContainer^  components;

	protected:

	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(main_window::typeid));
			this->close_button = (gcnew System::Windows::Forms::Button());
			this->transfer_from = (gcnew System::Windows::Forms::ComboBox());
			this->transfer_to = (gcnew System::Windows::Forms::ComboBox());
			this->transfer_from_value = (gcnew System::Windows::Forms::TextBox());
			this->program_name = (gcnew System::Windows::Forms::Label());
			this->label_from = (gcnew System::Windows::Forms::Label());
			this->label_to = (gcnew System::Windows::Forms::Label());
			this->convert = (gcnew System::Windows::Forms::Button());
			this->hide_button = (gcnew System::Windows::Forms::Button());
			this->transfer_to_result = (gcnew System::Windows::Forms::TextBox());
			this->update_info = (gcnew System::Windows::Forms::Label());
			this->update_label = (gcnew System::Windows::Forms::Label());
			this->tray_bar = (gcnew System::Windows::Forms::NotifyIcon(this->components));
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->SuspendLayout();
			// 
			// close_button
			// 
			this->close_button->BackColor = System::Drawing::Color::Indigo;
			this->close_button->Cursor = System::Windows::Forms::Cursors::Default;
			this->close_button->FlatAppearance->MouseOverBackColor = System::Drawing::Color::Red;
			this->close_button->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->close_button->Location = System::Drawing::Point(365, 0);
			this->close_button->Name = L"close_button";
			this->close_button->Size = System::Drawing::Size(35, 21);
			this->close_button->TabIndex = 0;
			this->close_button->TabStop = false;
			this->close_button->Text = L"X";
			this->close_button->UseVisualStyleBackColor = false;
			this->close_button->Click += gcnew System::EventHandler(this, &main_window::close_button_Click);
			// 
			// transfer_from
			// 
			this->transfer_from->BackColor = System::Drawing::SystemColors::Window;
			this->transfer_from->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->transfer_from->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->transfer_from->ForeColor = System::Drawing::SystemColors::InfoText;
			this->transfer_from->FormattingEnabled = true;
			this->transfer_from->Location = System::Drawing::Point(79, 63);
			this->transfer_from->Name = L"transfer_from";
			this->transfer_from->Size = System::Drawing::Size(179, 21);
			this->transfer_from->TabIndex = 2;
			// 
			// transfer_to
			// 
			this->transfer_to->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->transfer_to->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->transfer_to->FormattingEnabled = true;
			this->transfer_to->Location = System::Drawing::Point(79, 114);
			this->transfer_to->Name = L"transfer_to";
			this->transfer_to->Size = System::Drawing::Size(179, 21);
			this->transfer_to->TabIndex = 3;
			// 
			// transfer_from_value
			// 
			this->transfer_from_value->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(65)),
				static_cast<System::Int32>(static_cast<System::Byte>(64)), static_cast<System::Int32>(static_cast<System::Byte>(64)));
			this->transfer_from_value->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
			this->transfer_from_value->ForeColor = System::Drawing::SystemColors::Info;
			this->transfer_from_value->Location = System::Drawing::Point(12, 63);
			this->transfer_from_value->Name = L"transfer_from_value";
			this->transfer_from_value->Size = System::Drawing::Size(61, 20);
			this->transfer_from_value->TabIndex = 1;
			this->transfer_from_value->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &main_window::transfer_from_value_KeyPress);
			// 
			// program_name
			// 
			this->program_name->AutoSize = true;
			this->program_name->Font = (gcnew System::Drawing::Font(L"Candara", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->program_name->ForeColor = System::Drawing::SystemColors::Info;
			this->program_name->Location = System::Drawing::Point(9, 4);
			this->program_name->Name = L"program_name";
			this->program_name->Size = System::Drawing::Size(114, 17);
			this->program_name->TabIndex = 9;
			this->program_name->Text = L"��������� �����";
			// 
			// label_from
			// 
			this->label_from->AutoSize = true;
			this->label_from->ForeColor = System::Drawing::SystemColors::ScrollBar;
			this->label_from->Location = System::Drawing::Point(76, 47);
			this->label_from->Name = L"label_from";
			this->label_from->Size = System::Drawing::Size(80, 13);
			this->label_from->TabIndex = 10;
			this->label_from->Text = L"��������� ��:";
			// 
			// label_to
			// 
			this->label_to->AutoSize = true;
			this->label_to->ForeColor = System::Drawing::SystemColors::ScrollBar;
			this->label_to->Location = System::Drawing::Point(76, 98);
			this->label_to->Name = L"label_to";
			this->label_to->Size = System::Drawing::Size(74, 13);
			this->label_to->TabIndex = 11;
			this->label_to->Text = L"��������� �:";
			// 
			// convert
			// 
			this->convert->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->convert->ForeColor = System::Drawing::SystemColors::Info;
			this->convert->Location = System::Drawing::Point(279, 88);
			this->convert->Name = L"convert";
			this->convert->Size = System::Drawing::Size(103, 23);
			this->convert->TabIndex = 4;
			this->convert->Text = L"��������������";
			this->convert->UseVisualStyleBackColor = true;
			this->convert->Click += gcnew System::EventHandler(this, &main_window::convert_Click);
			// 
			// hide_button
			// 
			this->hide_button->FlatStyle = System::Windows::Forms::FlatStyle::Flat;
			this->hide_button->Location = System::Drawing::Point(337, 0);
			this->hide_button->Name = L"hide_button";
			this->hide_button->Size = System::Drawing::Size(31, 21);
			this->hide_button->TabIndex = 13;
			this->hide_button->TabStop = false;
			this->hide_button->Text = L"---";
			this->hide_button->UseVisualStyleBackColor = true;
			this->hide_button->Click += gcnew System::EventHandler(this, &main_window::hide_button_Click);
			// 
			// transfer_to_result
			// 
			this->transfer_to_result->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(64)),
				static_cast<System::Int32>(static_cast<System::Byte>(64)), static_cast<System::Int32>(static_cast<System::Byte>(64)));
			this->transfer_to_result->BorderStyle = System::Windows::Forms::BorderStyle::None;
			this->transfer_to_result->ForeColor = System::Drawing::SystemColors::ScrollBar;
			this->transfer_to_result->Location = System::Drawing::Point(79, 152);
			this->transfer_to_result->Name = L"transfer_to_result";
			this->transfer_to_result->ReadOnly = true;
			this->transfer_to_result->Size = System::Drawing::Size(179, 13);
			this->transfer_to_result->TabIndex = 16;
			this->transfer_to_result->TabStop = false;
			// 
			// update_info
			// 
			this->update_info->AutoSize = true;
			this->update_info->ForeColor = System::Drawing::SystemColors::Info;
			this->update_info->Location = System::Drawing::Point(127, 23);
			this->update_info->Name = L"update_info";
			this->update_info->Size = System::Drawing::Size(0, 13);
			this->update_info->TabIndex = 17;
			// 
			// update_label
			// 
			this->update_label->AutoSize = true;
			this->update_label->ForeColor = System::Drawing::SystemColors::ScrollBar;
			this->update_label->Location = System::Drawing::Point(9, 23);
			this->update_label->Name = L"update_label";
			this->update_label->Size = System::Drawing::Size(112, 13);
			this->update_label->TabIndex = 18;
			this->update_label->Text = L"���������� ������:";
			// 
			// tray_bar
			// 
			this->tray_bar->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"tray_bar.Icon")));
			this->tray_bar->Text = L"��������� �����";
			this->tray_bar->Visible = true;
			this->tray_bar->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &main_window::tray_bar_MouseClick);
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Interval = 50;
			this->timer1->Tick += gcnew System::EventHandler(this, &main_window::timer1_Tick);
			// 
			// main_window
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(64)), static_cast<System::Int32>(static_cast<System::Byte>(64)),
				static_cast<System::Int32>(static_cast<System::Byte>(64)));
			this->ClientSize = System::Drawing::Size(398, 185);
			this->Controls->Add(this->update_label);
			this->Controls->Add(this->update_info);
			this->Controls->Add(this->transfer_to_result);
			this->Controls->Add(this->hide_button);
			this->Controls->Add(this->convert);
			this->Controls->Add(this->label_to);
			this->Controls->Add(this->label_from);
			this->Controls->Add(this->program_name);
			this->Controls->Add(this->transfer_from_value);
			this->Controls->Add(this->transfer_to);
			this->Controls->Add(this->transfer_from);
			this->Controls->Add(this->close_button);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Name = L"main_window";
			this->ShowInTaskbar = false;
			this->StartPosition = System::Windows::Forms::FormStartPosition::Manual;
			this->Text = L"��������� �����";
			this->Load += gcnew System::EventHandler(this, &main_window::main_window_Load);
			this->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &main_window::main_window_MouseDown);
			this->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &main_window::main_window_MouseMove);
			this->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &main_window::main_window_MouseUp);
			this->ResumeLayout(false);
			this->PerformLayout();

		}


#pragma endregion
		String^ separator; // ����� ��� �������
		XmlDocument^ doc = gcnew XmlDocument();
		bool newdata = false;
		bool conn = false;

		void GetCourses(int d, int m, int y)
		{
			String^ds = d.ToString();
			String^ ms = m.ToString();
			String^ ys = y.ToString();
			if (d < 10) ds = "0" + ds;
			if (m < 10) ms = "0" + ms;
			String^ url = String::Format("http://www.cbr.ru/scripts/XML_daily.asp?date_req={0}/{1}/{2}", ds, ms, ys);
			HttpWebRequest^ req = (HttpWebRequest^)WebRequest::Create(url);
			req->Method = "GET";
			req->UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:45.0) Gecko/20100101 Firefox/45.0";
			req->Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";
			req->Headers->Add("Accept-Language", "ru - RU, ru; q = 0.8, en - US; q = 0.5, en; q = 0.3");
			req->Host = "cbr.ru";
			req->Referer = "http://www.cbr.ru/";

			IPStatus status = IPStatus::Unknown;
			try
			{
				status = (gcnew Ping())->Send("cbr.ru")->Status;
			}
			catch (...) {}

			if (status == IPStatus::Success)
			{
				HttpWebResponse^ response = nullptr;
				response = (HttpWebResponse^)req->GetResponse();
				StreamReader^ sr = gcnew StreamReader(response->GetResponseStream(), System::Text::Encoding::Default);
				String^ s = sr->ReadToEnd();
				doc->LoadXml(s);
				conn = true;
			}
		}

		double findElem(String^ str)
		{
			double num = 1;
			XmlDocument^ docf = gcnew XmlDocument();
			docf->Load("Data.xml");
			XmlElement^ fRoot = docf->DocumentElement;
			if (str != "RUB")
			{
				int j = fRoot->ChildNodes->Count;
				for (int i = 0; i < j; i++)
				{
					XmlElement^ el1 = (XmlElement^)fRoot->GetElementsByTagName("Valute")[i];
					if (((XmlElement^)el1->GetElementsByTagName("CharCode")[0])->InnerText == str)
					{
						num = Convert::ToDouble(((XmlElement^)el1->GetElementsByTagName("Value")[0])->InnerText);
						int nominal = Convert::ToInt32(((XmlElement^)el1->GetElementsByTagName("Nominal")[0])->InnerText);
						num /= nominal;
						return num;
					}
				}
			}
			return num;
		}

		void Conver()
		{
			if (transfer_from_value->Text != "" && transfer_from->SelectedItem != nullptr && transfer_to->SelectedItem != nullptr)
			{
				double num = Convert::ToDouble(transfer_from_value->Text);
				String^ strin = (String^)transfer_from->SelectedItem;
				strin = strin->Remove(3);
				String^ strout = (String^)transfer_to->SelectedItem;
				strout = strout->Remove(3);
				double numin = findElem(strin), numout = findElem(strout);
				double answer = num * (numin / numout);
				answer = Math::Round(answer, 4);
				transfer_to_result->Text = num + " " + strin + " = " + answer + " " + strout;
			}
			else
			{
				transfer_to_result->Text = "������� ������";
			}
		}

	private: System::Void main_window_Load(System::Object^  sender, System::EventArgs^  e) {
		separator = Globalization::NumberFormatInfo::CurrentInfo->NumberDecimalSeparator; //����������, ������� ��� �����

		System::Drawing::Size resolution = System::Windows::Forms::Screen::PrimaryScreen->Bounds.Size;
		Location = Point(resolution.Width - 415, resolution.Height - 230);

		XmlDocument^ docf = gcnew XmlDocument();
		docf->Load("Data.xml");
		XmlElement^ fRoot = docf->DocumentElement;
		String^ datefil = fRoot->GetAttribute("Date");
		update_info->Text = datefil;
		transfer_from->Items->Add("RUB-���������� �����");
		transfer_to->Items->Add("RUB-���������� �����");
		int j = fRoot->ChildNodes->Count;
		for (int i = 0; i < j; i++)
		{
			XmlElement^ el1 = (XmlElement^)fRoot->GetElementsByTagName("Valute")[i];
			String^ name = ((XmlElement^)el1->GetElementsByTagName("Name")[0])->InnerText;
			String^ code = ((XmlElement^)el1->GetElementsByTagName("CharCode")[0])->InnerText;
			transfer_from->Items->Add(code + "-" + name);
			transfer_to->Items->Add(code + "-" + name);
		}

		DateTime^ datesystem = DateTime::Now;
		int days = datesystem->Day;
		int months = datesystem->Month;
		int years = datesystem->Year;
		String^ ds = days.ToString();
		String^ ms = months.ToString();
		String^ ys = years.ToString();
		if (days < 10) ds = "0" + ds;
		if (months < 10) ms = "0" + ms;
		String^ datesys = ds + "." + ms + "." + ys;
		if (datesys == datefil)
		{
			newdata = true;
			update_info->Text = "�������";
			timer1->Enabled = false;
		}

		StreamReader^ sr = gcnew StreamReader("Start.txt");
		String^ str = sr->ReadLine();
		if (str != "null") { transfer_from_value->Text = str; }
		else { transfer_from_value->Text = ""; }
		str = sr->ReadLine();
		if (str != "null") { transfer_from->Text = str; }
		else { transfer_from->Text = ""; }
		str = sr->ReadLine();
		if (str != "null") { transfer_to->Text = str; }
		else { transfer_to->Text = ""; }
		sr->Close();

	}
	private: System::Void close_button_Click(System::Object^  sender, System::EventArgs^  e) {
		StreamWriter^ sw;
		FileInfo^ file = gcnew FileInfo("Start.txt");
		sw = file->CreateText();
		if (transfer_from_value->Text != "") { sw->WriteLine(transfer_from_value->Text); }
		else { sw->WriteLine("null"); }
		if (transfer_from->Text != "") { sw->WriteLine(transfer_from->Text); }
		else { sw->WriteLine("null"); }
		if (transfer_to->Text != "") { sw->WriteLine(transfer_to->Text); }
		else { sw->WriteLine("null"); }
		sw->Close();
		Close();
	}

	private: System::Void hide_button_Click(System::Object^  sender, System::EventArgs^  e)
	{
	Hide();
	}
	private: System::Void transfer_from_value_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e)
	{
		bool sep_found = false; // �������������� ���� ������
		if (Char::IsDigit(e->KeyChar) == true)  return;
		if (e->KeyChar == (char)Keys::Back) return;
		if ((transfer_from_value->Text->IndexOf(separator) != -1))
			sep_found = true;
		if (sep_found == true) { e->Handled = true; return; }
		if ((e->KeyChar.ToString() == separator)&&(transfer_from_value->Text->Length!=0)) return;
		e->Handled = true; //handled=true - ������ ������� KeyPress
	}
	private: System::Void convert_Click(System::Object^  sender, System::EventArgs^  e) 
	{
		if (newdata)
		{
			Conver();
		}
		else
		{
			if (conn)
			{
				doc->Save("Data.xml");
				newdata = true;
				Conver();
			}
			else
			{
				Conver();
			}
		}
	}
	private: System::Void tray_bar_MouseClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
	{
		Show();
		Activate();
	}

	private: bool isMouseDown = false; 
	private: Point mouseOffset;
	private: System::Void main_window_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) 
	{
		int xOffset, yOffset; //���������� ��� �����������
		if (e->X < 337 && e->Y < 35) //������� �� �������������� ����
		{
			Opacity = .6;
			xOffset = -e->X;
			yOffset = -e->Y;
			mouseOffset = System::Drawing::Point(xOffset, yOffset); 
			isMouseDown = true;
		}
	}
	private: System::Void main_window_MouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
	{
		if (isMouseDown)
		{
			Point mousePos = Control::MousePosition;
			mousePos.Offset(mouseOffset.X, mouseOffset.Y);
			Location = mousePos;
		}
	}
	private: System::Void main_window_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e)
	{
		isMouseDown = false;
		Opacity = 1;
	}
	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) 
	{
		timer1->Interval = 30000;
		DateTime datesystem = DateTime::Now;
		int days = datesystem.Day;
		int months = datesystem.Month;
		int years = datesystem.Year;
		GetCourses(days, months, years);
		if (conn)
		{
			timer1->Enabled = false;
			update_info->Text = "�������";
		}
	}
};
}
