#include "main_window.h"
#include <Windows.h>
using namespace System;
using namespace System::Windows::Forms;

[STAThread]
int main(array<String^>^ arg) {
	FreeConsole();
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	FreeConsole();
	conv::main_window form;
	Application::Run(%form);
	return 0;
}
